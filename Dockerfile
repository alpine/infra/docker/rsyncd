FROM registry.alpinelinux.org/img/alpine:3.21

RUN apk add --no-cache rsync

ENTRYPOINT [ "rsync", "--daemon", "--no-detach", "--config=/etc/rsyncd.conf" ]
